package core.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.Vector;

public class Config {

	private String path = System.getProperty("user.dir")+File.separator+"config"+File.separator+"otrsorter.properties";
	private String showpatch = System.getProperty("user.dir")+File.separator+"config"+File.separator+"otrsorter-shows.txt";
	
	private Properties config;
	
	public Config(){
		config = new Properties();
		if(!new File(System.getProperty("user.dir")+File.separator+"config").exists()){
			new File(System.getProperty("user.dir")+File.separator+"config").mkdir();
		}
		if(!new File(path).exists() || !new File(showpatch).exists()){
			try {
				new File(path).createNewFile();
				new File(showpatch).createNewFile();
				addshow("Abenteuer Leben");
				addshow("heute show");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		syncShowSerieList();
		try {
			config.load(new FileInputStream(new File(path)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private boolean syncShowSerieList() {
		
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new URL("https://raw.githubusercontent.com/Duell10111/otrd-lists/master/tv-shows_serien.txt").openStream(), "UTF-8"));
			Vector<String> names = new Vector<>();
			String line = "";
			while((line = reader.readLine()) != null) {
				names.add(line);
			}
			addallshow(names);
			return true;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public void save(String punkt, String wert){
		config.setProperty(punkt, wert);
		try {
			config.store(new FileOutputStream(new File(path)), "OTR Sorter Config");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String load(String punkt){
		return config.getProperty(punkt);
	}
	
	public Properties getProp(){
		return config;
	}
	
	public Vector<String> getshownames(){
		try {
			Vector<String> tmp = new Vector<String>();
			BufferedReader reader = new BufferedReader(new FileReader(new File(showpatch)));
			String line = "";
			while((line = reader.readLine())!= null){
				tmp.add(line);
			}
			reader.close();
			return tmp;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public synchronized void addshow(String name){
		if(getshownames().contains(name))
			return;
		try {
			PrintWriter writer = new PrintWriter(new FileWriter(new File(showpatch),true));
			writer.println(name);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void addallshow(Vector<String> names) {
		if(getshownames().containsAll(names))
			return;
		
		try {
			PrintWriter writer = new PrintWriter(new FileWriter(new File(showpatch),true));
			for(String s : names) {
				writer.println(s);
				writer.flush();
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
