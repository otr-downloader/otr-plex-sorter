package core.main;

import java.io.File;
import java.sql.SQLException;
import java.util.Locale;

import core.sorter.FileDB;
import core.sorter.FileRenamer;
import core.sorter.MusicDB;
import core.sorter.MusicSorter;

public class OTRSorter {

	private static Config conf;
	private static FileDB filedb;
	private static MusicDB musicDB;
	
	public static void main(String[] args) {
		new OTRSorter();
	}

	public OTRSorter(){
		//Locale.setDefault(Locale.GERMANY);
		//System.setProperty("file.encoding", "UTF-8");
		conf = new Config();
		if(conf.getProp().isEmpty()){
			conf.save("desdir", System.getProperty("user.dir"));
			conf.save("tvshowsfolder", System.getProperty("user.dir"));
			conf.save("srcfolder", System.getProperty("user.dir"));
			conf.save("dbfile", System.getProperty("user.dir"));
			conf.save("musicdbfile", System.getProperty("user.dir"));
			conf.save("musicsrcfolder", System.getProperty("user.dir"));
			conf.save("musicfolder", System.getProperty("user.dir"));
			conf.save("musicunsupportedfolder", System.getProperty("user.dir"));
			System.out.println("!!!! CHANGE OUTPUT AND INPUT FOLDER IN CONFIG !!!!");
			System.exit(0);
		}else{
			while(true) {
				try {
					filedb = new FileDB();
					musicDB = new MusicDB();
					break;
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
			new MSearchThread().start();
			while(true){
				if(new File(conf.load("srcfolder")).listFiles().length != 0){
					new FileRenamer().sort();
				}
				try {
					Thread.sleep(120000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static Config getConf(){
		return conf;
	}

	public static FileDB getFiledb() {
		return filedb;
	}

	public static MusicDB getMusicDB() {
		return musicDB;
	}
	
	class MSearchThread extends Thread{
		public void run(){
			while(true){
				if(new File(conf.load("musicsrcfolder")).listFiles().length != 0){
					new MusicSorter().sort();
				}
				try {
					Thread.sleep(120000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
