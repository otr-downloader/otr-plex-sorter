package core.sorter;

import java.io.File;
import java.io.IOException;

public class ProcessRunner {

	/**
	 * @deprecated
	 * @param dir
	 */
	public void convertencoding(File dir){
		
		if(!isLinuxSystem()){
			return;
		}
		
		try {
			Process process = Runtime.getRuntime().exec("convmv -f iso-8859-15 -t utf-8 --notest -r "+dir.getAbsolutePath());
			System.out.println("Convert Encoding");
			process.waitFor();
			System.out.println("Conert Encoding finished");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	 public static boolean isLinuxSystem()
	  { String osName = System.getProperty("os.name").toLowerCase();
	    return osName.indexOf("linux") >= 0;
	  }
	
}
