package core.sorter;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Vector;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import core.main.OTRSorter;


public class FileRenamer {
	
	private File desfolder = new File(OTRSorter.getConf().load("desdir"));
	private File desshowsfolder = new File(OTRSorter.getConf().load("tvshowsfolder"));
	private File sourcefolder = new File(OTRSorter.getConf().load("srcfolder"));

	public void sort() {

		for (File f : sourcefolder.listFiles()) {
			if (f.isDirectory()) { // Unterordner max. 1!
				for (File f2 : f.listFiles()) {
					sortFile(f2);
				}
			} else { // Oberordner
				sortFile(f);
			}
		}
	}

	private boolean isShow(String name) {
		for (String t : OTRSorter.getConf().getshownames()) {
			if (t.equals(name)) {
				return true;
			}
		}
		return false;
	}
	
	private void sortFile(File f) {
		long time = System.currentTimeMillis() - 20000;
		if(f.lastModified() > time){
			return; //Datei wird erst nach 20 Sekunden nicht bearbeitung beachtet
		}
		if(f.getName().contains(".mp3")) { //MP3 Sperre
			return;
		}
		if (f.renameTo(f) && !f.getName().contains(".part")) {
			FileInfo info = getInfos(f);
			Path p = Paths.get(f.getAbsolutePath());
			//Erstellt die erforderlichen Ordner
			if (OTRSorter.getFiledb().exists(f.getName())) {
				try {
					if (info.istserie()) {
						new File(OTRSorter.getFiledb().load(f.getName()).getString(6) + File.separator
								+ "Staffel " + String.format("%02d", info.getStaffel())).mkdirs();
					} else if (isShow(info.getTitle())) {
						DateFormat date = new SimpleDateFormat("yy");
						String season = date.format(new Date());
						new File(OTRSorter.getFiledb().load(f.getName()).getString(6) + File.separator
								+ "Staffel " + season).mkdirs();
					} else {
						new File(OTRSorter.getFiledb().load(f.getName()).getString(6)).mkdirs();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {

				if (info.istserie()) { //Ist eine Serie
					new File(desshowsfolder.getAbsolutePath() + File.separator + info.getTitle()
							+ File.separator + "Staffel " + String.format("%02d", info.getStaffel())).mkdirs();
				} else if (isShow(info.getTitle())) { //Showname == wird in Showordner abgelegt
					DateFormat date = new SimpleDateFormat("yy");
					String season = date.format(new Date());
					new File(desshowsfolder.getAbsolutePath() + File.separator + info.getTitle()
							+ File.separator + "Staffel " + season).mkdirs();
				} else { //Film
					new File(desfolder.getAbsolutePath() + File.separator + info.getTitle()).mkdirs();
				}
			}
			String quality = "";
			switch (info.quality) {
			case 4:
				quality = "720p";
				break;
			case 3:
				quality = "561p";
				break;
			case 2:
				quality = "360p";
				break;
			case 1:
				quality = "150p";
				break;
			}
			Path ptarget = null;
			String ort = null;
			if (OTRSorter.getFiledb().exists(f.getName())) {
				try {
					if (info.istserie()) {
						ptarget = Paths
								.get(OTRSorter.getFiledb().load(f.getName()).getString(6) + File.separator
										+ "Staffel " + String.format("%02d", info.getStaffel()) + File.separator
										+ info.getTitle() + " - s" + String.format("%02d", info.getStaffel())
										+ "e" + String.format("%02d", info.getEpisode()) + " " + quality + "."
										+ info.getFiletyp());
						ort = OTRSorter.getFiledb().load(f.getName()).getString(6);
					} else if (isShow(info.getTitle())) {
						DateFormat date = new SimpleDateFormat("yy");
						String season = date.format(new Date());

						int length = new File(OTRSorter.getFiledb().load(f.getName()).getString(6)
								+ File.separator + "Staffel " + season).listFiles().length;

						ptarget = Paths.get(OTRSorter.getFiledb().load(f.getName()).getString(6)
								+ File.separator + "Staffel " + season + File.separator + info.getTitle()
								+ " - s" + season + "e" + String.format("%02d", length + 1) + " " + quality
								+ "." + info.getFiletyp());
						ort = OTRSorter.getFiledb().load(f.getName()).getString(6);
					} else {
						ptarget = Paths.get(OTRSorter.getFiledb().load(f.getName()).getString(6)
								+ File.separator + info.getTitle() + " - " + quality + "." + info.getFiletyp());
						ort = OTRSorter.getFiledb().load(f.getName()).getString(6);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				if (info.istserie()) {
					ptarget = Paths.get(
							desshowsfolder.getAbsolutePath() + File.separator + info.getTitle() + File.separator
									+ "Staffel " + String.format("%02d", info.getStaffel()) + File.separator
									+ info.getTitle() + " - s" + String.format("%02d", info.getStaffel()) + "e"
									+ String.format("%02d", info.getEpisode()) + " " + quality + "."
									+ info.getFiletyp());
					ort = desshowsfolder.getAbsolutePath() + File.separator + info.getTitle();
				} else if (isShow(info.getTitle())) {
					DateFormat date = new SimpleDateFormat("yy");
					String season = date.format(new Date());

					int length = new File(desshowsfolder.getAbsolutePath() + File.separator + info.getTitle()
							+ File.separator + "Staffel " + season).listFiles().length;

					ptarget = Paths.get(desshowsfolder.getAbsolutePath() + File.separator + info.getTitle()
							+ File.separator + "Staffel " + season + File.separator + info.getTitle() + " - s"
							+ season + "e" + String.format("%02d", length + 1) + " " + quality + "."
							+ info.getFiletyp());
					ort = desshowsfolder.getAbsolutePath() + File.separator + info.getTitle();
				} else {
					ptarget = Paths.get(desfolder.getAbsolutePath() + File.separator + info.getTitle()
							+ File.separator + info.getTitle() + " - " + quality + "." + info.getFiletyp());
					ort = desfolder.getAbsolutePath() + File.separator + info.getTitle();
				}
			}
			try {
				if (ptarget.toFile().exists()) {
					/*int returned = JOptionPane.showConfirmDialog(null,
							"Soll die Datei " + info.getTitle() + " überschrieben werden ?",
							"Datei existiert bereits", JOptionPane.YES_NO_OPTION);
					if (returned == JOptionPane.YES_OPTION) {
						Files.move(p, ptarget, StandardCopyOption.ATOMIC_MOVE);
					}*/
					System.err.println("Datei existiert bereits im Zielort");
				} else {
					Files.move(p, ptarget, StandardCopyOption.ATOMIC_MOVE);
					//permission(ort);
					ptarget.toFile().setExecutable(true, false);
					ptarget.toFile().setReadable(true, false);
					ptarget.toFile().setWritable(true, false);
					OTRSorter.getFiledb().save(info.getTitle(), info.istserie(), info.getStaffel(),
							info.getEpisode(), ort);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Waiting");
		}
	}

	private FileInfo getInfos(File f) {
		String filename = f.getName();

		String[] splited = filename.split("_");
		int id = -1;
		for (int i = 0; i < splited.length; i++) {
			if (Pattern.matches("\\d{2}\\.\\d{2}\\.\\d{2}", splited[i])) {
				id = i;
			}
		}
		int id2 = id;
		boolean serie = false;
		int sesson = -1, episode = -1;
		if (splited[id - 1].matches("S\\d{2}E\\d{2}")) {
			serie = true;
			String split[] = splited[id - 1].split("\\p{Alpha}");
			sesson = Integer.parseInt(split[1]);
			episode = Integer.parseInt(split[2]);
			id2--;
		}

		String name = "";
		for (int i = 0; i < id2; i++) {
			if (i != id2 - 1) {
				name = name.concat(splited[i] + " ");
			} else {
				name = name.concat(splited[i]);
			}
		}
		System.out.println(name);
		int quality = 0;
		Vector<String> back= new Vector<String>(Arrays.asList(splited[splited.length - 1].split("\\.")));
		int index = -1;
		if(back.contains("HD")){
			quality = 4;
		}else if(back.contains("HQ")){
			quality = 3;
		}else if((index = back.indexOf("mp4")) >= 1 && back.get(index-1).equals("mpg")){
			quality = 1;
		}else if (back.contains("avi")){
			quality = 2;
		}
		String[] ende = splited[splited.length - 1].split("\\.");
		String endung = ende[ende.length - 1];

		if (serie) {
			return new FileInfo(name, quality, serie, episode, sesson, endung);
		}
		return new FileInfo(name, quality, serie, -1, -1, endung);
	}
	
	private void permission(String ort){
		if(isLinuxSystem()){
			try {
				System.out.println(ort);
				Process pr = Runtime.getRuntime().exec("chmod 777 -R "+ort);
				new Printer(pr.getInputStream());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	class Printer extends Thread{
		private InputStream in;
		public Printer(InputStream in){
			this.in= in;
		}
		public void run(){
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			@SuppressWarnings("unused")
			String line = "";
			try {
				while((line = reader.readLine())!= null){
					System.out.println(in);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static boolean isLinuxSystem()
	  { String osName = System.getProperty("os.name").toLowerCase();
	    return osName.indexOf("linux") >= 0;
	  }

	class FileInfo {

		private String Title;
		private int quality;
		private boolean serie;
		private int staffel;
		private int episode;
		private String filetyp;

		public FileInfo(String t, int Quality, boolean serie, int episode, int staffel, String typ) {
			Title = t;
			this.quality = Quality;
			this.serie = serie;
			this.setEpisode(episode);
			this.setStaffel(staffel);
			this.setFiletyp(typ);
		}

		public String getTitle() {
			return Title;
		}

		public int getquality() {
			return quality;
		}

		public boolean istserie() {
			return serie;
		}

		public int getStaffel() {
			return staffel;
		}

		public void setStaffel(int staffel) {
			this.staffel = staffel;
		}

		public int getEpisode() {
			return episode;
		}

		public void setEpisode(int episode) {
			this.episode = episode;
		}

		public String getFiletyp() {
			return filetyp;
		}

		public void setFiletyp(String filetyp) {
			this.filetyp = filetyp;
		}

	}

}
