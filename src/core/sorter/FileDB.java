package core.sorter;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import core.main.OTRSorter;

public class FileDB {

	private Connection connection;
	
	public FileDB() throws SQLException{
		boolean exists = false;
		if(!new File(OTRSorter.getConf().load("dbfile")).exists())
			exists = true;
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:"+OTRSorter.getConf().load("dbfile"));
			System.out.println("Database connection succesfully");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(exists) {
			Statement stmt = connection.createStatement();
			stmt.execute("CREATE TABLE `Files` ("
					+ "`ID`	INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ "`Name`	TEXT NOT NULL,"
					+ "`Serie`	INTEGER NOT NULL,"
					+ "`Staffel`	INTEGER,"
					+ "`Episode`	INTEGER,"
					+ "`Speicherort`	TEXT NOT NULL"
					+ ");");
			//Speicher intelligente Nummerierung ?
			stmt.close();
		}
	}
	
	public synchronized void save(String name, boolean Serie, int Staffel, int Episode, String Speicherort){
		
		try {
			Statement stmp = connection.createStatement();
			
			String sql = "INSERT INTO Files (ID,Name,Serie,Staffel,Episode,Speicherort) VALUES (NULL,'"+name+"',"+(Serie ? 1:0)+","+Staffel+","+Episode+",'"+Speicherort+"');";
			
			stmp.executeUpdate(sql);
			stmp.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ResultSet load(String name){
		try {
			Statement stmp = connection.createStatement();
			
			String sql = "SELECT * FROM Files WHERE Name='"+name+"';";
			ResultSet rs = stmp.executeQuery(sql);
			
			return rs;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public boolean exists(String name){
		try {
			ResultSet rs = load(name);
			int anzahl = 0;
			while(rs.next()){
				anzahl ++;
			}
			if(anzahl > 0){
				return true;
			}else{
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
}
