package core.sorter;

import java.io.File;
import java.io.IOException;
import com.mpatric.mp3agic.ID3v1;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;

public class MP3Infos {

	public Mp3File getMp3File(File f){
		try {
			String n = new String(f.getAbsolutePath().getBytes("Cp1252"),"Cp1252");
			System.out.println("UTF String : "+n);
			Mp3File mfile = new Mp3File(f);
			return mfile;
		}catch (UnsupportedTagException e) {
			e.printStackTrace();
		} catch (InvalidDataException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String getTitle(Mp3File f){
		if(f.hasId3v2Tag()){
			ID3v2 id3v2 = f.getId3v2Tag();
			return id3v2.getTitle();
		}else if(f.hasId3v1Tag()){
			ID3v1 id3v1 = f.getId3v1Tag();
			return id3v1.getTitle();
		}
		return null;
	}
	
	public String getAlbum(Mp3File f){
		if(f.hasId3v2Tag()){
			ID3v2 id3v2 = f.getId3v2Tag();
			return id3v2.getAlbum();
		}else if(f.hasId3v1Tag()){
			ID3v1 id3v1 = f.getId3v1Tag();
			return id3v1.getAlbum();
		}
		return null;
	}
	
	public String getArtist(Mp3File f){
		if(f.hasId3v2Tag()){
			ID3v2 id3v2 = f.getId3v2Tag();
			return id3v2.getArtist();
		}else if(f.hasId3v1Tag()){
			ID3v1 id3v1 = f.getId3v1Tag();
			return id3v1.getArtist();
		}
		return null;
	}
}
