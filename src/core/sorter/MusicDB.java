package core.sorter;

import java.sql.Statement;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import core.main.OTRSorter;

public class MusicDB {

	private Connection connection;
	
	public MusicDB() throws SQLException{
		boolean exists = true;
		if(!new File(OTRSorter.getConf().load("musicdbfile")).exists())
			exists = false;
			
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:"+OTRSorter.getConf().load("musicdbfile"));
			System.out.println("Database connection succesfully");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(!exists) {
			Statement statement = connection.createStatement();
			statement.execute("CREATE TABLE `Music` (" + 
					//"`ID`	INTEGER PRIMARY KEY AUTOINCREMENT," + 
					"`Title`	TEXT," + 
					"`Album`	TEXT," + 
					"`Artist`	TEXT," + 
					"`Speicherort`	TEXT NOT NULL," + 
					" PRIMARY KEY(`Title`,`Album`,`Artist`,`Speicherort`)" +
					");");
			statement.close();
		}
	}
	
	public void save(String title, String album, String artist, String Speicherort){
		
		try {
			String sql = "INSERT INTO Music (Title,Album,Artist,Speicherort) VALUES (?,?,?,?)";
			
			PreparedStatement execStatement = connection.prepareStatement(sql);
			
			execStatement.setString(1, title);
			execStatement.setString(2, album);
			execStatement.setString(3, artist);
			execStatement.setString(4, Speicherort);
			
			execStatement.executeUpdate();
			execStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ResultSet load(String Title,String Album,String Artist){
		try {
			Statement stmp = connection.createStatement();
			
			String sql = "SELECT * FROM Music WHERE Title='"+Title+"' AND Album='"+Album+"' AND Artist='"+Artist+"';";
			ResultSet rs = stmp.executeQuery(sql);
			
			return rs;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public boolean exists(String Title,String Album,String Artist){
		try {
			ResultSet rs = load(Title,Album,Artist);
			int anzahl = 0;
			while(rs.next()){
				anzahl ++;
			}
			if(anzahl > 0){
				return true;
			}else{
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
}
