package core.sorter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Vector;

import com.mpatric.mp3agic.Mp3File;

import core.main.OTRSorter;

public class MusicSorter {

	private MP3Infos mp3Infos;
	private ProcessRunner runner;

	public MusicSorter() {
		mp3Infos = new MP3Infos();
		runner = new ProcessRunner();
	}

	public void sort() {
		File dir = new File(OTRSorter.getConf().load("musicfolder"));
		File src = new File(OTRSorter.getConf().load("musicsrcfolder"));

		//runner.convertencoding(src);
		
		for (File f : scandir(src)) {
			
			Mp3File mfile = mp3Infos.getMp3File(f);
			if(mfile == null){
				System.out.println("Keine MP3Tags");
				try {
					Files.move(f.toPath(), new File(OTRSorter.getConf().load("musicunsupportedfolder")+File.separator+f.getName()).toPath(), StandardCopyOption.ATOMIC_MOVE);
				} catch (IOException e) {
					e.printStackTrace();
				}
				continue;
			}
			if (mfile.hasId3v2Tag() || mfile.hasId3v1Tag()) {

				File desdir = new File(dir.getAbsolutePath() + File.separator + mp3Infos.getArtist(mfile)
						+ File.separator + mp3Infos.getAlbum(mfile));
				desdir.mkdirs();
				File desfile = new File(desdir.getAbsolutePath() + File.separator + f.getName());
				if (!desfile.exists()) {
					try {
						Files.move(f.toPath(), desfile.toPath(), StandardCopyOption.ATOMIC_MOVE);
						OTRSorter.getMusicDB().save(mp3Infos.getTitle(mfile), mp3Infos.getAlbum(mfile), mp3Infos.getArtist(mfile), desfile.getAbsolutePath());
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					f.delete();
				}
			}
		}
	}

	private Vector<File> scandir(File dir) {
		Vector<File> tmp = new Vector<File>();
		for (File f : dir.listFiles()) {
			if (f.isDirectory()) {
				tmp.addAll(scandir(f));
			} else {
				if(f.getName().contains(".part")){
					continue;
				}
				tmp.add(f);
			}
		}
		return tmp;
	}

}
